'user strict';

var eventApp = angular.module('eventApp', ['ngRoute', 'ngAnimate', 'ngResource'])
        .config(['$routeProvider', '$locationProvider', function($routeProvider,$locationProvider) {
          $routeProvider
            .when('/', {
              templateUrl: 'views/main.html',
              controller: 'MainCtrl'
            })
            .when('/subcategories', {
              templateUrl: 'views/subcategories.html',
              controller: 'SubcategoriesCtrl'
            })
            .when('/subcategories/ads/:subcategory_id', {
              templateUrl: 'views/subcategories_ads.html',
              controller: 'SubcategoriesAdsCtrl'
            })
            .otherwise({
              redirectTo: '/'
            });
            
            //$locationProvider.html5Mode(true)
        }]);

  eventApp.constant('settings', {
      urlBase: 'http://192.241.250.86'
  });


  eventApp.factory('Ad', ['$resource', 'settings', function($resource, settings){
    return $resource(settings.urlBase+'/api/subcategories/ads/:subcategory_id');
  }]);

  eventApp.factory('Subcategory', ['$resource', 'settings', function($resource, settings){
    return $resource(settings.urlBase+'/api/subcategories');
  }]);