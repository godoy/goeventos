'use strict';

angular.module('eventApp')
    .controller('SubcategoriesAdsCtrl', ['$scope', '$routeParams', 'Ad', function($scope, $routeParams, Ad) {
      $scope.heading = "Go Events";

      $scope.ads = Ad.query({subcategory_id:$routeParams["subcategory_id"]})
      //subcategory_id
    }]);
