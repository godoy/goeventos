'use strict';

angular.module('eventApp')
    .controller('SubcategoriesCtrl', ['$scope', 'Ad', 'Subcategory', function($scope, Ad, Subcategory) {
      $scope.heading = "Go Events";
      $scope.subcategories = Subcategory.query();
    }]);
    